// ===================
// Input
// ===================
node_address = process.argv[3].split("=")[1];
if ( typeof node_address === 'undefined' )
{
    console.log("Missing node address");
    return 1;
}

contract_name = process.argv[4].split("=")[1];
if ( typeof contract_name === 'undefined' )
{
    console.log("Missing contract name");
    return 1;
}

contract_file_path = "./" + contract_name + ".sol";

contract_param_1="0x00Ee7508D62B152C3413C55807052644714ba36D";
contract_param_2="Testing RaiseToSummon an entity";
contract_param_3=3600;
contract_params=[contract_param_1, contract_param_2, contract_param_3];


// ===================
// Tests
// ===================
function tests() {
    describe('owner', function() {
        it('expected to match', function() {
            assert.equal("0x8833af1a9b1e7ddc837ed2c771078dcf3ac23daf", contract_instance.owner());
        });
    });

    describe('receiver', function() {
        it('expected to match', function() {
            assert.equal("0x00ee7508d62b152c3413c55807052644714ba36d", contract_instance.receiver());
        });
    });

    describe('cause', function() {
        it('expected to match', function() {
            assert.equal("Testing RaiseToSummon an entity", contract_instance.cause());
        });
    });

    describe('expirationInSeconds', function() {
        it('expected to match', function() {
            assert.equal("3600", contract_instance.expirationInSeconds());
        });
    });

    describe('hasBeenClaimed', function() {
        it('expected to match', function() {
            assert.equal(false, contract_instance.hasBeenClaimed());
        });
    });

    describe('numPayments', function() {
        it('expected to match', function() {
            assert.equal("0", contract_instance.numPayments());
        });
    });

    describe('totalAmountRaised', function() {
        it('expected to match', function() {
            assert.equal("0", contract_instance.totalAmountRaised());
        });
    });
}

// ===================
// Entrypoint
// ===================
var devkit = require('/home/devkit/tools/devkit.js');
devkit.test(contract_params, tests);

// TODO: consider extracting/compressing code
//module.exports = {
//    tests: tests
//}
