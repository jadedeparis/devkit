local cjson = require 'cjson'
local reqargs = require 'reqargs'
local ws_server = require "resty.websocket.server"
local posix = require "posix"
local pwait = require "posix.sys.wait"
local fcntl = require "posix.fcntl"
local unistd = require "posix.unistd"
local lyaml = require("lyaml")


local Routes = {GET={},POST={}}
local _M = {}

function Wrapper()
    local _files = {}

    local function send(id, data)
        -- Add data to buffer.
        _files[id].buffer = _files[id].buffer .. data
    end

    local function _close(id)
        if _files[id].isPipe then
           local pfd = _files[id].isPipe
           unistd.close(pfd.fd)
           --for i = 1, #pfd.pids - 1 do
               --pwait.wait(pfd.pids[i])
           --end
           --ngx.log(ngx.ERR, "PID", pfd.pids[1])
           --local pid, msg, status = pwait.wait(pfd.pids[#pfd.pids])

            local pid, msg, status = posix.pclose(_files[id].isPipe)
            --ngx.log(ngx.ERR, "return", pid, msg, status)
            _files[id].status = type(status) == "number" and status or 99
            _files[id].fd = nil
        else
            unistd.close(_files[id].fd)
            _files[id].status = 0
            _files[id].fd = nil
        end
    end

    local function invalidate()
        for id, File in pairs(_files) do
            if File.fd then
                _close(id)
            end
        end
    end

    local function write(id)
        -- Write from buffer all that we can out on file.
        local fd = _files[id].fd
        local s = _files[id].buffer
        while true do
            local success, count = pcall(unistd.write, fd, s)
            if not success then
                invalidate()
                return
            end
            if count == nil or count == 0 then
                break
            end
            s = s:sub(count + 1)
            if #s == 0 then
                break
            end
        end
        _files[id].buffer = s
    end

    local function read(id, size)
        -- Read from file into buffer.
        size = size or 1024
        local fd = _files[id].fd
        local s = ""
        while true do
            local data = unistd.read(fd, size > 0 and size or 1024)
            if data == nil or #data == 0 then
                break
            end
            s = s .. data
            if size > 0 and #s >= size then
                break
            end
        end
        _files[id].buffer = _files[id].buffer .. s
    end

    local function readLine(id)
         --Read a line in blocking mode and return it.
         --Bypasses the buffer.
        local f = fcntl.fcntl(_files[id].fd, fcntl.F_SETFL, fcntl.O_BLOCK)
        if not f then
            log("Error: could not BLOCK fd.")
            os.exit(99)
        end
        local line = ""
        while true do
            local byte = unistd.read(_files[id].fd, 1)
            if byte == "\n" then
                break
            end
            line = line .. byte
        end
        local f = fcntl.fcntl(_files[id].fd, fcntl.F_SETFL, fcntl.O_NONBLOCK)
        if not f then
            log("Error: could not NONBLOCK fd.")
            os.exit(99)
        end
        return line
    end

    local function recv(id)
        -- Return buffered data and clear it.
        local data = _files[id].buffer
        _files[id].buffer = ""
        return data
    end

    local function isEmpty(id)
        return #_files[id].buffer == 0
    end

    local function getPoll(fds)
        local count = 0
        for id, File in pairs(_files) do
            if File.fd then
                File.events = {}
                File.revents = nil
                if File.mode == "r" then
                    if #File.buffer < 1024*10 then
                        File.events.IN = true
                    end
                elseif File.mode == "w" then
                    if #File.buffer > 0 then
                        File.events.OUT = true
                    end
                end
                count = count + 1
                fds[File.fd] = File
            end
        end
        return count
    end

    local function isValid()
        -- Returns true if any file is still open.
        for id, File in pairs(_files) do
            if File.fd then return true end
        end
        return false
    end

    local function status(id)
        return _files[id].status
    end

    local function postPoll()
        local hadHup = false
        for id, File in pairs(_files) do
            if File.fd then
                if File.revents.IN then
                    read(id, 0)
                elseif File.revents.OUT then
                    write(id)
                end
                if File.revents.HUP then
                    _close(id)
                    hadHup = true
                elseif File.revents.ERR then
                    _close(id)
                    hadHup = true
                end
            end
        end
        if hadHup then
            invalidate()
        end
    end

    local function newFile(id, fd, mode, isPipe)
        _files[id] = {
            id = id,
            fd = fd,
            buffer = "",
            mode = mode,
            isPipe = isPipe,
        }
        local f = fcntl.fcntl(_files[id].fd, fcntl.F_SETFL, fcntl.O_NONBLOCK)
        if f == -1 then
            log("Error: could not NONBLOCK fd.")
            os.exit(99)
        end
    end

    return {
        send = send,
        write = write,
        read = read,
        readLine = readLine,
        recv = recv,
        isEmpty = isEmpty,
        getPoll = getPoll,
        postPoll = postPoll,
        newFile = newFile,
        isValid = isValid,
        invalidate = invalidate,
        status = status,
    }
end

function _M._response(obj, status)
    status = status or 200
    ngx.header.content_type = "application/json"
    local body = cjson.encode(obj or {})
    ngx.status = status
    ngx.print(string.format("%s\n", body))
end

function _M._route()
    local params={}
    params.get, params.post, params.files = reqargs({})
    params.header = ngx.req.get_headers()
    params.uri = ngx.var.uri
    params.location = ngx.var.location
    params.path = params.uri:sub(#params.location)
    params.request_uri = ngx.var.request_uri
    params.method = ngx.req.get_method():upper()
    --_M._response(params)
    --if 1 then return end

    local routes = Routes[params.method] or {}
    local fn
    local matches
    ngx.log(ngx.ERR, "path", params.path)
    for routePath,route in pairs(routes) do
        ngx.log(ngx.ERR, "rouePath", routePath)
        matches = {params.path:match(routePath)}
        if matches[1] ~= nil then
            fn = route[1]
            break
        end
    end

    if not fn then
        _M._response(nil, 404)
    else
        fn(matches, params)
    end
end

-- List all projects, return the directory names.
function _M.getProjects()
    -- List all directories in projects home dir which have a Dappfile.yaml inside of them.
    local cmd="find /home/devkit/projects/ -type f -name Dappfile.yaml -maxdepth 2 | cut -d/ -f5"
    local fd = io.popen(cmd)
    if not fd then
        _M._response(nil, 500)
        return
    end
    local lines = fd:read("*a")
    fd:close()
    local response={}
    for dir in lines:gmatch("[^\n]*") do
        if #dir > 0 then
            local fd = io.open(string.format("%s/%s/Dappfile.yaml", "/home/devkit/projects", dir))
            local contents = fd:read("*a")
            fd:close()
            local success, dappfile = pcall(lyaml.load, contents)
            if success and dappfile then
                response.projects = response.projects or {}
                table.insert(response.projects, {dappfile=dappfile,type="project",dir=dir})
            end
        end
    end
    _M._response(response)
end

function _M.initProject(matches, params)
    local executable = "sh"
    --local args={"-c", string.format('SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /init/ -e projectshome=/home/devkit/projects -e project=%s -e template=%s,project, template')}
    local project=matches[1]
    local template=params.get.template or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /init/ -e projectshome=/home/devkit/projects -e project=%s -e template=%s', project, template)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.makeProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /make/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.compileProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /compile/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.testProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /test/ -e container= -e projectshome=/home/devkit/projects -e project=%s', project)
    ngx.log(ngx.ERR, args_space)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.buildProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /build/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    ngx.log(ngx.ERR, args_space)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.infoProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /info/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    ngx.log(ngx.ERR, args_space)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.eventsProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /events/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    ngx.log(ngx.ERR, args_space)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.buildJSProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root /usr/local/bin/space -m gitlab.com/web3-sh/devkit /js/ -e container= -e projectshome=/home/devkit/projects -e project=%s -- %s', project, posargs)
    ngx.log(ngx.ERR, args_space)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.webMakeProject(matches, params)
    local project=matches[1]
    local posargs=params.get.posargs or ""
    local executable = "sh"
    local args_space = string.format('PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin SPACE_LOG_ENABLE_COLORS=0 HOME=/root ; ( cd /home/devkit/projects/%s && /usr/local/bin/space /app/make/ -e container= -- %s )', project, posargs)
    ngx.log(ngx.ERR, args_space)
    local args={"-c", args_space}
    _M.execute(executable, args)
end

function _M.webTestProject(matches, params)
    local executable = "./towley.sh"
    local args={"aaffe"}
    _M.execute(executable, args)
end

function _M.execute(executable, args)
    local proxy = Wrapper()

    local stdoutfdr,stdoutfdw = unistd.pipe()
    proxy.newFile(1, stdoutfdr, "r")

    local stderrfdr,stderrfdw = unistd.pipe()
    proxy.newFile(2, stderrfdr, "r")

    local stdinfdr,stdinfdw = unistd.pipe()
    local fd1 = posix.dup(1)
    local fd2 = posix.dup(2)
    posix.dup2(stdoutfdw, 1)
    posix.dup2(stderrfdw, 2)
    local pfd = posix.popen({executable, args}, "w", function() return stdinfdr, stdinfdw end)
    proxy.newFile(0, stdinfdw, "w", pfd)
    posix.dup2(fd1, 1)
    posix.dup2(fd2, 2)
    posix.close(fd1)
    posix.close(fd2)

    if not pfd then
        ngx.log(ngx.ERR, "Error popen for process.")
        ngx.exit(403)
        return
    end

    function ws()
        -- https://github.com/openresty/lua-resty-websocket
        local wb, err = ws_server:new{
            timeout = 5000,  -- in milliseconds
            max_payload_len = 65535,
        }
        if not wb then
            ngx.log(ngx.ERR, "ws: failed to create new socket: ", err)
            return false
        end

        while true do
            local fds = {}
            local c2 = proxy.getPoll(fds)
            if c2 == 0 then
                break
            end

            posix.poll(fds, -1)
            proxy.postPoll()

            -- Move data from proxy to ws.
            local toSend=""
            local data = proxy.recv(1)
            if #data > 0 then
                toSend = string.format("%s1\n%d\n%s", toSend, #data, data)
            end
            local data = proxy.recv(2)
            if #data > 0 then
                toSend = string.format("%s2\n%d\n%s", toSend, #data, data)
            end
            if not proxy.isValid() then
                -- The proxy has ended, send the status code on ws.
                toSend = string.format("%s0\n%d\n", toSend, proxy.status(0))
            end

            while #toSend > 0 do
                local bytes, err = wb:send_text(toSend)
                if not bytes then
                    ngx.log(ngx.ERR, "ws: failed to send a text frame: ", err)
                    return false
                end
                break
            end
            if not proxy.isValid() then
                break
            end
        end

        local bytes, err = wb:send_close(1000)
        if not bytes then
            ngx.log(ngx.ERR, "ws: failed to send the close frame: ", err)
            return false
        end

        return true
    end

    if not ws() then
        ngx.exit(444)
        return
    end

    ngx.exit(200)
end

Routes.GET["^/(projects)/$"] = {_M.getProjects}
Routes.GET["^/project/(.+)/init/$"] = {_M.initProject}
Routes.GET["^/project/(.+)/make/$"] = {_M.makeProject}
Routes.GET["^/project/(.+)/compile/$"] = {_M.compileProject}
Routes.GET["^/project/(.+)/build/$"] = {_M.buildProject}
Routes.GET["^/project/(.+)/info/$"] = {_M.infoProject}
Routes.GET["^/project/(.+)/events/$"] = {_M.eventsProject}
Routes.GET["^/project/(.+)/js/$"] = {_M.buildJSProject}
Routes.GET["^/project/(.+)/test/$"] = {_M.testProject}
Routes.GET["^/project/(.+)/app_make/$"] = {_M.webMakeProject}
--Routes.GET["^/project/(.+)/webtest/$"] = {_M.webTestProject}

_M._route()
