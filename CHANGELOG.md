# Space Module change log - web-sh/devkit

## [0.8.0 - 2018-01-18]

+ Add initial tests to _NewsFeed_ template project

+ Add `- account: USER1` to Dappfile

* Change `PROJECT_INIT` to consider `umask`

* Fix permissions settings in `/init/`

* Change project arguments to deploy directory for viewing

* Update project templates

* Update default _CORS_ configuration to consider _MetaMask_

* Update web application


## [0.7.0 - 2018-01-17]

+ Add _Lua_ to _nginx_

+ Add web application

+ Add `/info/`

+ Add `-e apis` to `/run/`

+ Add `/events/`

* Solve various static analysis warnings

* Update module help text information and description

* Fix typo in module help text information

* Rename default home project directory

* Rename default container name

* Fix bug for exporting JS

* Update code documentation

* Update `/add` templates

* Allow `/test/` to use `./contracts` as default directory


## [0.6.0 - 2018-01-15]

+ Add INSTALL file

+ Add pre-installed DevKit Space module to Dockerfile

+ Add error checking to `BUILD_ETHEREUM_LOCAL`

* Change _Block Explorer_ relative links to consider /explorer/ server route

* Update instructions on README

* Solve various static analysis warnings

* Update code documentation

* Update README to include install instructions

* Fix wrong path for explorer configuration in Dockerfile

* Modify _YAML_ to not allow `RUN` parameters to be overridden


## [0.5.2 - 2018-01-11]

* Move `remixd` initialization from Dockerfile to entrypoint.sh

* Move _Block Explorer_ initialization from Dockerfile to entrypoint.sh


## [0.5.1 - 2018-01-11]

* Fix wrong path used for Remix configuration


## [0.5.0 - 2018-01-11]

+ Include /add_args/

+ Add timestamp feature in `/compile/` (skip rebuild by default)

+ Add `-e recompile` option to `/compile/`

+ Add support for Dapp specification file: `Dappfile.yaml`

+ Add support for accessing shared projects directory directly in the _Remix IDE_ with _Remixd_

+ Add `-e remixdport` option to `/run/`

+ Add `COMPILE_ETHEREUM` function

+ Add `DAPPFILE_CONTRACT` functions

+ Add `BUILD_WAIT` function

+ Add `BUILD_ETHEREUM` functions

+ Add `/js/`

+ Add `/stop/`

+ Add `/make/`

+ Add ability for contracts to reside in sub-directories

+ Add `-e rebuild` option to `/build/`

+ Add block explorer

* Change single line commands to proper if construct

* Update documentation

* Move abi generation from `/compile/` to `WEB_BUILD` step

* Move js helper files from `/compile/` to `WEB_BUILD` step

* Solve various static analysis warnings

* Change `break` keyword to non-zero `return` in `/add_args/`

* Update `/compile/` to consider `Dappfile.yaml` specification

* Change module file extension from `.sh` to `.bash`

* Change `stat` command in favor of `FILE_STAT`

* Change all `exit` statements to `return` keyword

* Rename `COMPILE` to `COMPILE_PROJECT`

* Change `COMPILE` implementation to support Dappfile specification

* Change `BUILD` implementation to support Dappfile specification

* Double quote condition parameters to prevent word splitting

* Split up single-line local variables declaration to prevent masking return values

* Reorganize project target structure

* Update _MyEtherWallet_ (MEW) to version 3.11.2.1

* Rename `/deploy/` to `/build/`

* Modify `/init/` to work with multiple templates

* Rename default template from `dapp` to `raiseforacause`

* Update Remix to build d50dcc4

- Remove arguments support from `/compile/`

- Remove `ipc` argument from /compile/

- Remove `contract` argument from /compile/

- Remove `contract` argument from /build/

- Remove `gas` argument from /build/

- Remove `account` argument from /build/


## [0.4.2 - 2017-12-26]

* Fix shebang lines for all tools


## [0.4.1 - 2017-12-26]

* Update _MyEtherWallet_ (MEW) to version 3.11.1.2

* Update _Solidity_ compiler (solc) to version 0.4.19

* Update Remix to build f4e1f6c


## [0.4.0 - 2017-12-26]

+ Add `hotreload` option to `/test/`

+ Add `breakonfailure` option to `/test/`

+ Add improved error checking in `/deploy/`

+ Add `-e optimization` switch to `/compile/`

+ Add optional `-e arguments` to `/compile/` for appending constructor arguments

+ Add initial local testing

+ Add `jq` program to Dockerfile

+ Add `nginx` parameter to `/run/`

+ Add `Spacefile` to dapp template

+ Add check in `/compile/` to verify arguments count match expected constructor signature

+ Add CI tests

+ Add external programs availability check

+ Add `SPACE_ENV` setting for external programs

* Set `/test/` lower bound threshold to minimum value to always output execution time

* Change `/deploy/` control flow to exit with error when `.abi` or `.bin` files are missing

* Improve `contract_bin` definition in `/deploy/` to only prefix `0x` when needed

* Separate commands containing declaration and assignment on the same line

* Update documentation

* Change all target operations to work outside of container

* Update `/compile/` to create www/js directory when needed

* Fix bug that happens when setting the projects home directory

* Update instructions on README

* Fix module description typo

- Disable false positive shellcheck warnings

- Remove `ethereumjs-testrpc`

- Remove `docker` as dependency in `dep_install`


## [0.3.1 - 2017-11-28]

* Fix potential word splitting variable expansion

* Update `/test/` to use existing `.abi` and `.bin` files when available

* Move _solc_ js package out of the fast `devkit.js` code path

* Update Dockerfile for improved readability

* Update Dockerfile to explicitly declare build-only dependencies as virtual

* Update Dockerfile to explicitly ignore caching

- Remove unused variables


## [0.3.0 - 2017-11-27]

+ Add `/test/`

+ Add `devkit.js` tool for testing

+ Add tests for dapp template

* Fix bug that prevents empty or default genesis on `/run/`

* Update instructions on README

* Change Dockerfile to include `mocha` for testing

* Double quote condition parameters to prevent word splitting

- Disable false positive shellcheck warnings


## [0.2.0 - 2017-11-23]

+ Add `bump_version` helper script

+ Add `-e genesis` option for custom genesis file on `/run/`

+ Add support for `-e cors` option for configurable CORS policy

+ Add support for `-e corsip` option for configurable host IP address (CORS policy)

* Change preloaded list of Geth APIs to include `net` and `txpool`

* Change Dockerfile entrypoint to give included tools more flexibility (parametrization)

* Change default `cors` variable to include common `192.168.99.100` docker-machine IP.

- Remove bashisms in favor of sh/dash equivalents


## [0.1.1 - 2017-11-22]

* Update code documentation

* Update `geth` to version 1.7.3


## [0.1.0 - 2017-11-21]

+ Add working openresty

* General improvements to Space module

* General improvements to DApp template


## [0.0.6 - 2017-11-20]

+ Add code documentation

+ Add Ethereum units conversion helper

+ Add `/_dep_install/`

+ Add explicit `os` module dependency

* Update dapp template

* Update README

* Update Dockerfile to include openresty


## [0.0.5 - 2017-11-17]

+ Add `image_version` environment variable

* Fix template contract constructor


## [0.0.4 - 2017-11-15]

* Improve several functions and services

* Update instructions

* Optimize Docker image for size


## [0.0.3 - 2017-11-15]

* Update _MyEtherWallet_ (MEW) to version 3.10.7.1

* Update _Solidity_ compiler (solc) to version 0.4.18


## [0.0.2 - 2017-11-15]

+ Add space-requirement settings

* Change `Dockerfile` to have better prepare for software version updates


## [0.0.1 - 2017-11-15]

+ Initial version
