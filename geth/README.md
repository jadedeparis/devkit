# Genesis.json
We are providing a simple `genesis.json` file to run a private net using Proof of Autority (no CPU cycle burning) and some preseeded accounts.

## Testnet
To run against a public testnet you will have to override the `docker run` command, more on this later.
