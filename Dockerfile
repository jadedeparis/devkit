#
FROM registry.gitlab.com/space-sh/space:1.1.0
MAINTAINER Blockie

#
# Dependencies version description
#
ENV solc_version=0.4.19 \
    geth_version=1.7 \
    web3_version=0.20 \
    mew_version=3.11.2.1 \
    remix_version=d50dcc4

#
# OpenResty settings
#
ARG RESTY_VERSION="1.11.2.2"
ARG RESTY_OPENSSL_VERSION="1.0.2j"
ARG RESTY_PCRE_VERSION="8.39"
ARG RESTY_J="1"
ARG _RESTY_CONFIG_DEPS="--with-openssl=/tmp/openssl-${RESTY_OPENSSL_VERSION} --with-pcre=/tmp/pcre-${RESTY_PCRE_VERSION}"
ARG RESTY_CONFIG_OPTIONS="\
    --with-file-aio \
    --with-http_addition_module \
    --with-http_auth_request_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_geoip_module=dynamic \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_image_filter_module=dynamic \
    --with-http_mp4_module \
    --with-http_random_index_module \
    --with-http_realip_module \
    --with-http_secure_link_module \
    --with-http_slice_module \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_sub_module \
    --with-http_v2_module \
    --with-http_xslt_module=dynamic \
    --with-ipv6 \
    --with-mail \
    --with-mail_ssl_module \
    --with-md5-asm \
    --with-pcre-jit \
    --with-sha1-asm \
    --with-stream \
    --with-stream_ssl_module \
    --with-threads \
    --without-http_ssi_module \
    --without-http_userid_module \
    --without-http_uwsgi_module \
    --without-http_scgi_module \
    --pid-path=/usr/local/openresty/nginx.pid \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --http-client-body-temp-path=/var/nginx/client_body_temp \
    --http-proxy-temp-path=/var/nginx/proxy_temp \
    --http-fastcgi-temp-path=/var/nginx/fastcgi_temp \
    "

RUN apk update && apk upgrade && \
    apk add --no-cache --virtual .build-deps build-base cmake gcc g++ gd-dev geoip-dev go libxslt-dev make musl-dev curl-dev boost-dev linux-headers perl-dev python readline-dev zlib-dev && \
    apk add --no-cache bash curl git jq nodejs nodejs-npm vim gd geoip libgcc libxslt texinfo zlib && \
    sed -i -E -e 's/include <sys\/poll.h>/include <poll.h>/' /usr/include/boost/asio/detail/socket_types.hpp && \
    cd /tmp && \
    curl -fSL https://www.openssl.org/source/openssl-${RESTY_OPENSSL_VERSION}.tar.gz -o openssl-${RESTY_OPENSSL_VERSION}.tar.gz && \
    tar xzf openssl-${RESTY_OPENSSL_VERSION}.tar.gz && \
    curl -kfSL https://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-${RESTY_PCRE_VERSION}.tar.gz -o pcre-${RESTY_PCRE_VERSION}.tar.gz && \
    tar xzf pcre-${RESTY_PCRE_VERSION}.tar.gz && \
    curl -fSL https://openresty.org/download/openresty-${RESTY_VERSION}.tar.gz -o openresty-${RESTY_VERSION}.tar.gz && \
    tar xzf openresty-${RESTY_VERSION}.tar.gz && \
    mkdir -p /var/nginx && \
    cd /tmp/openresty-${RESTY_VERSION} && \
    ./configure -j${RESTY_J} ${_RESTY_CONFIG_DEPS} ${RESTY_CONFIG_OPTIONS} && \
    make -j${RESTY_J} && \
    make -j${RESTY_J} install && \
    cd /tmp && \
    rm -rf \
        openssl-${RESTY_OPENSSL_VERSION} \
        openssl-${RESTY_OPENSSL_VERSION}.tar.gz \
        openresty-${RESTY_VERSION}.tar.gz openresty-${RESTY_VERSION} \
        pcre-${RESTY_PCRE_VERSION}.tar.gz pcre-${RESTY_PCRE_VERSION} && \
    cd / && \
    curl -LO https://github.com/ethereum/solidity/releases/download/v${solc_version}/solidity_${solc_version}.tar.gz && tar zxvf solidity_${solc_version}.tar.gz && \
    cd /solidity_${solc_version} && cmake -DCMAKE_BUILD_TYPE=Release -DTESTS=0 -DSTATIC_LINKING=1 && \
    make solc && install -s  solc/solc /usr/bin && \
    cd / && rm -rf solidity_${solc_version} && rm solidity_${solc_version}.tar.gz && \
    git clone --depth 1 --branch release/${geth_version} https://github.com/ethereum/go-ethereum && \
    cd go-ethereum && make geth && cp ./build/bin/geth /usr/local/bin && \
    cd / && rm -rf go-ethereum && \
    git clone --depth 1 https://github.com/maandree/libkeccak && \
    cd libkeccak && make && make install && cd .. && \
    git clone --depth 1 https://github.com/maandree/argparser && \
    cd argparser && make c && make install-c && cd .. && \
    git clone --depth 1 https://github.com/maandree/sha3sum && \
    cd sha3sum && make all && make install && \
    cd / && rm -rf sha3sum argparser libkeccak && \
    mkdir -p /home/devkit/ && \
    cd /home/devkit/ && npm install mocha remixd solc web3@${web3_version} && \
    sed -i.bak "s/var loopback = '127.0.0.1'/var loopback = '0.0.0.0'/" node_modules/remixd/src/websocket.js && \
    cd /home/devkit/ && curl -L -O https://github.com/kvhnuke/etherwallet/releases/download/v${mew_version}/etherwallet-v${mew_version}.zip && unzip etherwallet-v${mew_version}.zip && mv etherwallet-v${mew_version} myetherwallet && \
    rm etherwallet-v${mew_version}.zip && \
    cd /home/devkit/ && mkdir remix && cd remix && curl -L -O https://github.com/ethereum/browser-solidity/raw/gh-pages/remix-${remix_version}.zip && unzip remix-${remix_version}.zip && rm remix-${remix_version}.zip && \
    sed -i.bak "s/'ws:\/\/localhost:65520'/'ws:\/\/' + window.location.host + ':65520'/" build/app.js && \
    sed -i.bak "s/'ws:\/\/127.0.0.1:65520'/'ws:\/\/' + window.location.host + ':65520'/" build/app.js && \
    cd /home/devkit/ && git clone https://github.com/carsenk/explorer && cd explorer && npm install bower && ./node_modules/bower/bin/bower --allow-root install && npm install && \
    sed -i.bak "s/var GETH_HOSTNAME\t= \"localhost\";/var GETH_HOSTNAME\t= window.location.host;/" app/app.js && \
    sed -i.bak "s/base href=\"\/\">/base href=\"\/explorer\/\">/" app/index.html && \
    sed -i.bak "s/href\=\".\/\#\//href\=\"\/explorer\/\#\//" app/views/blockInfos.html app/views/chainInfos.html app/views/transactionInfos.html app/index.html && \
    sed -i.bak "s/href\=\"\/\#\//href\=\"\/explorer\/\#\//" app/views/main.html app/views/api/api.html && \
    apk del .build-deps && \
    rm -rf /var/cache/apk/*

#
# Install DevKit from local files
# and preload it by installing listed dependencies
COPY . /root/.space/space_modules/gitlab.com/web3-sh/devkit
RUN space -m gitlab.com/web3-sh/devkit /_dep_install/

#
# Work space
COPY geth /home/devkit/geth
COPY nginx /home/devkit/nginx
COPY tools /home/devkit/tools

ENTRYPOINT ["/home/devkit/tools/entrypoint.sh"]
WORKDIR /home/devkit/tools
# Pass arguments as JSON array ["",""] to ENTRYPOINT using CMD
# CMD ["",""]
